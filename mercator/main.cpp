#include <cmath>

#define _CRT_SECURE_NO_WARNINGS
#include <cstdio>

double myabs(double x)
{
    return x > 0 ? x : -x;
}

double mercator(double x, int maxreps = 255, double mindelta = 0)
{
    mindelta = myabs(mindelta);
    double
        t = (1.0 - x) / (1.0 + x), t0squared = t * t,
        res = 0, delta;

    for (int i = 1; i <= maxreps; ++i)
    {
        delta = t / (2.0 * i - 1);
        res += delta;
        if (myabs(delta) < mindelta)
        {
            break;
        }
        t *= t0squared;
    }
    return -2.0 * res;
}

void printCase(double x)
{
    printf("myln(%4.20lf) = %4.20lf\n", x, mercator(x));
    printf("  ln(%4.20lf) = %4.20lf\n\n", x, log(x));
}

int main(int argc, char * argv[])
{
    printCase(1);
    printCase(10);
    printCase(2.718281828459045235360);
    printCase(2.718281828459045235360 * 2.718281828459045235360);
}
