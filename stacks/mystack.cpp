#include <cstdio>

#include "mystack.h"

Stack::Stack()
{
    count = 0;
    for(int i = 0; i < MAXSTACK; ++i)
        data[i] = -1;

}

int Stack::get()
{
    int rez = -1;
    if(count!=0)
    {
        rez = data[count-1];
        data[count-1] = -1;
        --count;
    }
    if(rez == -1)
    {
        count = -1;
        printf(" Stack is empty\n");
    }
    return rez;
}

int Stack::put(int _val)
{
    int rez = -1;
    if(count == MAXSTACK)
    {
        printf(" Stack is full\n");
    }
    else
    {
        data[count++] = _val;
        rez = count;
    }
    return rez;
}

bool Stack::isEmpty() const
{
    if(count==0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Stack::isFull() const
{
    if(count == MAXSTACK)
        return true;
    else
        return false;
}
