#include "myqueue.h"

bool Queue::put(int _val)
{
    if (!insertion.isFull())
    {
        insertion.put(_val);
        return true;
    }
    else if (extraction.isEmpty())
    {
        shift();
        insertion.put(_val);
        return true;
    }
    return false;
}

int Queue::get()
{
    if (extraction.isEmpty())
    {
        shift();
    }
    return extraction.get();
}

bool Queue::isEmpty() const
{
    return insertion.isEmpty();
}

bool Queue::isFull() const
{
    return insertion.isFull() && !extraction.isEmpty();
}

void Queue::shift()
{
    while (!insertion.isEmpty())
    {
        extraction.put(insertion.get());
    }
}
