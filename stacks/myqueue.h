#pragma once

#include "mystack.h"

class Queue
{
public:
    Queue() = default;
    ~Queue() = default;

    /// <returns> true if successful </returns>
    bool put(int _val);
    int get();
    bool isEmpty() const;
    bool isFull() const;

private:
    Stack insertion, extraction;

    /// <summary>
    /// Move 'insertion' stack contents to
    /// 'extraction' stack to prepare for
    /// 'get' operation
    /// </summary>
    void shift();
};
