#include <initializer_list>
#include <iostream>

#include "myqueue.h"

void put(Queue & q, std::initializer_list<int> in)
{
    std::cout << "Inserting: ";
    for (const auto & it : in)
    {
        std::cout << it << (q.put(it) ? "...ok " : "...failed ");
    }
    std::cout << std::endl;
}

void get(Queue & q, int num)
{
    std::cout << "Getting " << num << " elements: ";
    for (int i = 0; i < num; ++i)
    {
        std::cout << q.get() << " ";
    }
    std::cout << std::endl;
}

int main(int argc, char * argv[])
{
    Queue q;

    put(q, {1, 2, 3});
    get(q, 2);
    put(q, {4, 5, 6, 7});
    put(q, {8, 9, 10, 11, 12, 13});
    get(q, 4);
    get(q, 2);
    put(q, {14, 15, 16});
    get(q, 4);

    return 0;
}
