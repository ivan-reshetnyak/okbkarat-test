#ifndef MYSTACK_H
#define MYSTACK_H

#include <stdio.h>

const int MAXSTACK = 10;

class Stack
{
    int count;
    int data[MAXSTACK];
public:
    Stack();
    int get();
    int put(int _val);
    bool isEmpty() const;
    bool isFull() const;
};

#endif // MYSTACK_H
